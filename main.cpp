/**
 * Так как задание тестовое, и каждая дополнительная строчка кода существенно снижает вероятность того, что кто-то
 * будет разбираться с тем, как всё работает, я введу ряд допущений/ограничений.
 *   1.  Со стороны UDP работаем с 4-ой версией IPv4.
 *   2.  В UDP сокет передается структура can_frame, проверка корректности can_frame не выполняется.
 *
 * Как всё работает:
 * Программа позволяет создавать любое количество каналов передачи данных вида:
 * UDPx -> CANy и CANy -> xxx.xxx.xxx.xxx:UDPz
 * где 'x' - номер UDP порта, который принимает данные и передает их на CAN интерфейс с номером 'y',
 * с другой стороны данные, принимаемые от CAN интерфейса с номером 'y', передаются на порт 'z' адреса
 * IPv4 xxx.xxx.xxx.xxx
 * 
 * Подобная пара каналов задается в виде:
 * ./udp2can <X>:<canY|vcanY>:<XXX.XXX.XXX.XXX>:<Z>
 */

#include <cstdio>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include <memory>

#include <cstdio>
#include <cstring>
#include <unistd.h>

#include <sys/epoll.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <net/if.h>
#include <sys/ioctl.h>

#include <linux/can.h>
#include <linux/can/raw.h>

using namespace std;

// Способ получать форматированную строку как в старом добром snprintf
template<typename ... Args>
string StringFormat( const string& format, Args ... args )
{
    size_t length = snprintf( nullptr, 0, format.c_str(), args ... );
    unique_ptr<char[]> buffer( new char[ length + 1 ] ); 
    
    snprintf( buffer.get(), length + 1, format.c_str(), args ... );
    return string( buffer.get(), buffer.get() + length );
}

class CUdpCanChannelArray {
public:
    
    CUdpCanChannelArray() {}
    ~CUdpCanChannelArray() { CloseChannels(); }
    // Разберем командную строку, чтобы получить каналы передачи данных UDP/CAN
    // формат представления ( сложных проверок не выполняем, чтобы не загромождать тестовый пример
    // нецелевым кодом, поэтому пробелы в описании конкретного канала не допустимы):
    // <Input UDP port>:<CAN port>:<Output IPv4 address>:<Output udp port>
    void ParseArgs( int argc, const char* argv[] ) throw( invalid_argument );

    // Открыть все каналы UDP/CAN и установить соединения.
    // Вызов блокирующий, завершается по получению сигнала SIGINT
    void OpenChannelsAndProcess() throw( runtime_error );

    // Закрыть все каналы
    void CloseChannels();

private:    
    // Класс обрабатывает конкретный канал udp-can-udp
    class CUdpCanChannel {
        public:
            CUdpCanChannel( int udpInPort, string&& canInterface, string&& udpOutIpv4, int udpOutPort ) :
                udpInPort( udpInPort ), canInterface( canInterface ), udpOutIpv4( udpOutIpv4 ), udpOutPort( udpOutPort ),
                udpInFd( -1 ), canInOutFd( -1 ), udpOutFd( -1 ) {}

        
        void Open() throw( runtime_error );
        void Close();

        int GetUdpInFd() const { return udpInFd; }
        int GetUdpOutFd() const { return udpOutFd; }
        int GetCanInOutFd() const { return canInOutFd; }

        private:
            // Номер UDP порта на чтение
            int udpInPort;
            // Имя CAN интерфейса
            string canInterface;
            // IPv4 адрес на запись
            string udpOutIpv4;
            // Номер UDP порта на запись
            int udpOutPort;
            // Файловый дескриптор UDP сокета на чтение
            int udpInFd;
            // Файловый дескриптор UDP сокета на запись
            int udpOutFd;
            // Файловый дескриптор CAN сокета
            int canInOutFd;

            // Открыть udp сокет на чтение
            void openInUdp() throw( runtime_error );
            // Открыть udp сокет на запись
            void openOutUdp() throw( runtime_error );
            // Открыть сокет для работы с can
            void openInOutCan() throw( runtime_error );
    };

    vector<CUdpCanChannel> udpCanChannelArray;

    // Перенаправляем данные из сокета в сокет
    static void redirectData( int srcFd, int dstFd )  throw( runtime_error );
};

void CUdpCanChannelArray::ParseArgs( int argc, const char* argv[] ) throw( invalid_argument )
{
    if( argc <= 1 ) {
        throw invalid_argument( "There weren't any channel descriptions passed to." );
    }
    // Не будем выполнять сложных проверок при разборе аргументов командной строки, чтобы не заграмождать код
    // данного тестового задания.
    // Если что-то пойдет не так, то полетит исключение invalid_argument
    udpCanChannelArray.reserve( argc - 1 );
    for( int i = 1; i < argc; ++i ) {
        stringstream ss( argv[i] );
        const char delimeter = ':';

        // Получим номер порта и проверим, что он не превышает границу максимального допустимого значения
        auto GetPort = [&]() -> int {
            string temp;
            if ( !getline( ss, temp, delimeter ) ) {
                throw invalid_argument(argv[i]);
            }
            int port = stoi( temp );
            if( port > 65535 ) {
                throw invalid_argument( StringFormat( "Port number %i is out of range.", port ) );
            }
            return port;
        };

        // просто получим строку.
        auto GetString = [&]() -> string {
            string temp;
            if ( !getline( ss, temp, delimeter ) ) {
                throw invalid_argument(argv[i]);
            }
            return temp;
        };

        int udpInPort = GetPort();
        string canInterface = GetString();
        string udpOutIpv4 = GetString();
        int udpOutPort = GetPort();

        udpCanChannelArray.emplace_back( CUdpCanChannel( udpInPort, move( canInterface ), move( udpOutIpv4 ), udpOutPort ) );
    }
}

void CUdpCanChannelArray::OpenChannelsAndProcess() throw( runtime_error )
{
    int epollFd = epoll_create1( EPOLL_CLOEXEC );
    if( epollFd < 0 ) {
        throw runtime_error( StringFormat( "epoll_create() ends with error: %s.", strerror( errno ) ) );
    }

    for( auto& channel : udpCanChannelArray ) {
        channel.Open();

        epoll_event ev;
        // Регистрируем отслеживание собития получения данных на сокетах InUdp и Can.
        ev.events = EPOLLIN;
        // Чтобы не выделять допоолнительную память под пользовательские структуры, запишем
        // входной и выходной дескрипторы в 64-ти битную переменную.
        ev.data.u64 = ( static_cast<uint64_t>( channel.GetUdpInFd() ) << 32 ) + channel.GetCanInOutFd();
        if( epoll_ctl( epollFd, EPOLL_CTL_ADD, channel.GetUdpInFd(), &ev ) != 0 ) {
            throw runtime_error( StringFormat( "epoll_ctl() ends with error for input udp: %s.", strerror( errno ) ) );
        }

        // Установим флаг, что источником данных является CAN интерфейс
        ev.data.u64 = ( static_cast<uint64_t>( channel.GetCanInOutFd() ) << 32 ) + channel.GetUdpOutFd();
        if( epoll_ctl( epollFd, EPOLL_CTL_ADD, channel.GetCanInOutFd(), &ev ) != 0 ) {
            throw runtime_error( StringFormat( "epoll_ctl() ends with error for input can: %s.", strerror( errno ) ) );
        }
    }

    // Ждем данные и перенаправляем их пока не поличим сигнал SIGINT
    while( true ) {
        const int EventListSize = 16;
        epoll_event eventList[EventListSize];
        int readyCount = epoll_wait( epollFd, eventList, EventListSize, -1 );
        if( readyCount > 0 ) {
            for( int i = 0; i < readyCount; ++i ) {
                if( eventList[i].events & EPOLLIN ) {
                    // Пришел пакет, перенаправим его.
                    int srcFd = eventList[i].data.u64 >> 32;
                    int dstFd = eventList[i].data.u64;
                    redirectData( srcFd, dstFd );
                } else if( eventList[i].events & ( EPOLLERR | EPOLLHUP ) ) {
                    CloseChannels();
                    throw runtime_error( StringFormat( "Error occured in socket's event: %d.", eventList[i].events ) );
                }
            }
        } else {
            CloseChannels();
            if( errno == EINTR ) {
                // Получили сигнал, завершаем работу.
                cout << ( "The udp2can says goodbye to you!") << endl;
                break;
            } else {
                throw runtime_error( StringFormat( "Error occured in epoll_wait(): %s.", strerror( errno ) ) );
            }
        }
    }
}

void CUdpCanChannelArray::CloseChannels()
{
    for( auto& channel : udpCanChannelArray ) {
        channel.Close();
    }
}

void CUdpCanChannelArray::redirectData( int srcFd, int dstFd )  throw( runtime_error )
{
    // Функция не будет прерывать работу udp2can в случае ошибки.
    // Мы можем сделать механизм регистрации пользовательских CallBack-ов, чтобы передать право ему
    // решать что делать, если мы не можем передать данные.
    
    // Размер буфера больше ожидаемого размера пакета, чтобы отслеживать ситуации с неправильной длиной
    const int MaxBufferLength = 2 * sizeof( can_frame );
    unsigned char buffer[MaxBufferLength];

    int size = read( srcFd, buffer, MaxBufferLength );
    if( size == -1 ) {
        cout << StringFormat( "Cannot read data from socket %d to socket %d: %s", srcFd, buffer, strerror( errno ) ) << endl;
        return;
    }
    if( size != sizeof( can_frame ) ) {
        cout << StringFormat( "Wrong format of packet. Should contains 16 bytes as a can_frame struct." ) << endl;
        return;
    }

    if( write( dstFd, buffer, size ) == -1 ) {
        cout << StringFormat( "Cannot write data to socket: %s.", strerror( errno ) ) << endl;
        return;
    }
}

void CUdpCanChannelArray::CUdpCanChannel::Open() throw( runtime_error )
{
    try {
        // Открываем сокеты
        openInUdp();
        openOutUdp();
        openInOutCan();

    } catch( ... ) {
        // что-то пошло не так, закроем все дескрипторы и кинем исключение дальше
        Close();
        throw;
    }
}

void CUdpCanChannelArray::CUdpCanChannel::Close()
{
    close( udpInFd );
    close( canInOutFd );
    close( udpOutFd );
    udpInFd = -1;
    canInOutFd = -1;
    udpOutFd = -1;
}

void CUdpCanChannelArray::CUdpCanChannel::openInUdp() throw( runtime_error )
{
    sockaddr_in udpInAddr;

    udpInFd =  socket( PF_INET , SOCK_DGRAM , 0 );
    if( udpInFd == -1 ) {
        throw runtime_error( StringFormat( "Cannot open input udp socket: %s.", strerror( errno ) ) );
    }

    memset( &udpInAddr, 0, sizeof( udpInAddr ) ); 
    udpInAddr.sin_family = AF_INET;
    udpInAddr.sin_addr.s_addr = INADDR_ANY;
    udpInAddr.sin_port = htons( udpInPort );

    if( bind( udpInFd, ( sockaddr * )&udpInAddr , sizeof( udpInAddr ) ) == -1 ) {
        throw runtime_error( StringFormat( "Cannot bind input udp socket to port %d: %s.", udpInPort, strerror( errno ) ) );
    }
}

void CUdpCanChannelArray::CUdpCanChannel::openOutUdp() throw( runtime_error )
{
    sockaddr_in udpOutAddr;

    udpOutFd =  socket( PF_INET , SOCK_DGRAM , 0 );
    if( udpOutFd == -1 ) {
        throw runtime_error( StringFormat( "Cannot open output udp socket: %s.", strerror( errno ) ) );
    }

    memset( &udpOutAddr, 0, sizeof( udpOutAddr ) ); 
    udpOutAddr.sin_family = AF_INET;
    udpOutAddr.sin_port = htons( udpOutPort );
    if ( inet_pton( AF_INET, udpOutIpv4.c_str(), &udpOutAddr.sin_addr ) <= 0 ) {
        throw runtime_error( StringFormat( "Cannot convert output UDP IPv4 address %s: %s.", udpOutIpv4.c_str(), strerror( errno ) ) );
    }

    // Используем connect, чтобы иметь возможность писать через write
    if ( connect( udpOutFd, reinterpret_cast<sockaddr*>( &udpOutAddr ), sizeof( udpOutAddr ) ) != 0 ) {
        throw runtime_error( StringFormat( "Cannot connect output UDP IPv4 address %s:%d: %s.", udpOutIpv4.c_str(), udpInPort, strerror( errno ) ) );
    }
}

void CUdpCanChannelArray::CUdpCanChannel::openInOutCan() throw( runtime_error )
{
    sockaddr_can canAddr;
    ifreq ifr;

    canInOutFd = socket( PF_CAN, SOCK_RAW, CAN_RAW );

    strcpy( ifr.ifr_name, canInterface.c_str() );
    ioctl( canInOutFd, SIOCGIFINDEX, &ifr );

    canAddr.can_family = AF_CAN;
    canAddr.can_ifindex = ifr.ifr_ifindex;

    if( bind( canInOutFd, (struct sockaddr *)&canAddr, sizeof( canAddr ) ) == -1 ) {
        throw runtime_error(  StringFormat( "Cannot bind can socket for interface %s: %s", canInterface.c_str(), strerror( errno ) ) );
    }  
}

int main( int argc, char* argv[] )
{
    try {
        CUdpCanChannelArray udpCanChannelArray;
        
        // Разбираем командную строку для инициализации каналов
        udpCanChannelArray.ParseArgs( argc, const_cast<const char**>( argv ) );

        // Запускаем перенаправление пакето ( метод блокирующий, завершается по сигналу SIGINT )
        udpCanChannelArray.OpenChannelsAndProcess();
    } catch( invalid_argument& e ) {
        cerr << StringFormat( "Invalid argument. %s\n", e.what() );
        cerr << StringFormat( "Usage: you should pass list of channel descriptions to aplication, each of which "
        "should has following format: <Input UDP port>:<CAN port>:<Output IPv4 address>:<Output udp port>\n"
        "Spaces in channel descripteions are forbidden.\n") << endl;
        return 1;
    } catch( exception& e ) {
        cerr << StringFormat( "Error: %s", e.what() ) << endl;
        return 1;
    }

    return 0;
}
