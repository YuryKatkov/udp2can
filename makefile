all:
	g++ main.cpp -Wno-format-security -g -ggdb -O0 -std=c++11 -o udp2can

clean:
	rm -rf *.o udp2can
