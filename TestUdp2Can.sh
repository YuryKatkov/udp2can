#!/bin/bash

# Суть теста в следующем
# Мы открываем NUMBER_OF_CHANNELS каналов при помощи нашего машрутизатора udp2can
# Далее мы формируем NUMBER_OF_PACKETS пакетов и начинаем их в произвольном порядке передавать в разные концы наших каналов,
# при этом данные, получаемые на противоположном конце мы будем записывать в соответствующий файл.
# По завершении теста у нас будет NUMBER_OF_CHANNELS * 2 файлов ( по числу концов каналов ), в каждом из которых должны быть одни и те же
# данные но в разных последовательностях.
# Отсортировав эти файлы и сравнив, мы должны получить идентичные результаты, что будет означать успешное прохождение теста.

################################
# Пользовательские переменные. #
################################

# Можно задать количество каналов для тестирования
NUMBER_OF_CHANNELS=${NUMBER_OF_CHANNELS:-4}
# Количество передаваемых пакетов
NUMBER_OF_PACKETS=${NUMBER_OF_PACKETS:-10}

############
# Функции. #
############

# Сохраним информацию о том, был загружен модуль или нет.
# Загрузим его, если модуль загружен не был.
function LoadModule()
{
    local ifName=$1
    
    eval export IfExisted_${ifName}="$(lsmod | grep "^${ifName}[[:space:]]" &> /dev/null && echo 1 || echo 0)"
    if eval [[ "\${IfExisted_${ifName}}" == "0" ]]; then 
        echo "load module ${ifName}"
        sudo modprobe ${ifName}
    fi
}

# Выгрузим модуль, если он до этого не был загружен
function UnloadModule()
{
    local ifName=$1

    if eval [[ "\${IfExisted_${ifName}}" == "0" ]]; then 
        echo "unload module ${ifName}"
        sudo modprobe -r ${ifName}
    fi
}

# Загрузим CAN интерфейсы ( какие необходимо )
function LoadCan()
{
    LoadModule can
    LoadModule can_raw
    LoadModule vcan
}

# Выгрузим CAN интерфейсы ( которые не были загружены до старта теста )
function UnloadCan()
{
    UnloadModule vcan
    UnloadModule can_raw
    UnloadModule can
}

# Получим список свободных портов на локальной машине
function GetUnusedPorts()
{
    # число свободных портов, которые нужно найти
    local portsCount=$1
    local unusedPorts=()

    # получим список занятых портов
    local usedPorts=( $(netstat -lptun 2>/dev/null | awk '{print $4}' | grep :  | sed 's/.*://' | sort -n) )
    # получим верхнюю и нижнюю границу диапазона доступных портов
    read lowerPort upperPort < /proc/sys/net/ipv4/ip_local_port_range
    # найдем доступные для использования порты
    for ((i = 0 ; i < ${#usedPorts[@]} ; i++)); do
        # Пропускаем порты, которые меньше чем lowerPort
        if (( ${usedPorts[i]} > $lowerPort )); then
            #Нашли наименьший занятый порт, попадающий в диапазон
             for ((p = $lowerPort ; p <= ${upperPort} ; p++)); do
                if (( $p != ${usedPorts[i]} )); then
                    # нашли незанятый порт
                    unusedPorts+=($p)
                    # остановимся нашли нужное количество портов
                    (( portsCount-= 1 )) || { echo ${unusedPorts[@]}; return; }
                else
                    (( i < ${#usedPorts[@]} )) && (( i+= 1 ))
                fi
             done
        fi
    done
}

# Получим список свободных vcan интерфейсов
function GetUnusedCanInfs()
{
    # число свободных vcan итерфейсов, которые нужно найти
    local ifsCount=$1
    # Занятые номера vcan интерфейсов
    local usedIfsNumbers=( $(ip link show | sed -n 's/.* vcan\([0-9]\+\):.*/\1/p') )
	# Список свободных vcan интерфейсов, который мы будем заполнять
    local unusedIfs=()

	# Индекс элемента в usedIfsNumbers
    local usedIdx=0
	# Текущий проверяемый номер интерфейса
    local ifNumber=0

    while true; do
        if (( $usedIdx < ${#usedIfsNumbers[@]} )) && (( $ifNumber == ${usedIfsNumbers[usedIdx]} )); then
			# Если мы перебрали ещй не все занятые интерфейсы, а текущий проверяемый номер присутствует в списке занятых интерфейсов
            (( ifNumber+=1 ))
            (( usedIdx+=1 ))
        else
			# Номер свободен, добавим его в список и проверим, а нужно ли нам ещё искать номера
            unusedIfs+=($ifNumber)
            (( ifNumber+=1 ))
            (( ifsCount-=1 )) || break
        fi
    done
    echo ${unusedIfs[@]}
}

# список процессов, запущенных в фоне, которые нужно убить
BG_PIDS=()
# список поднятых CAN интерфейсов, которые нужно закрыть
CanIfs=()

function EndTest()
{
    echo "=== Kill processes launched in background ==="
    for pid in "${BG_PIDS[@]}"; do 
        echo "kill $pid"
        kill -0 $pid && kill -9 $pid
    done

    echo "=== Delete CAN interfaces ==="
    for i in "${CanIfs[@]}"; do 
        sudo ip link set down vcan$i
        sudo ip link delete dev vcan$i type vcan
    done

    # выгрузим модули CAN ( елси они не были загружены до теста )
    echo "=== Unload CAN modules if necessary ==="
    UnloadCan
}

function OnError()
{
    echo "ERROR OCCURED!"
    EndTest
    exit 1
} 
#######################################################################################################################
##########################
# Основное тело скрипта. #
##########################

set -e

# При возникновении ошибки закроем тест
trap OnError ERR

while true; do
    read -p "In this script you will have to enter sudo password for creating virtual CAN port. Do you trust to this script? [y/N]: " agreement
    agreement=${agreement:-N}
    
    if [[ "${agreement}" == "N" ]] || [[ "${agreement}" == "n" ]]; then
        echo "I'm sorry but automatic test isn't impossible. :("
        exit 1
    elif [[ "${agreement}" == "Y" ]] || [[ "${agreement}" == "y" ]]; then
        echo "Here we go!"
        break
    else
        echo -e "\\n!!! Wrong answer. Please try again.\\n"
    fi
done

# 1. --- Соберем пример
echo "=== Build udp2can ==="
g++ main.cpp -Wno-format-security -std=c++11 -o udp2can

# 2. --- Загрузим модули CAN
echo "=== Load CAN modules if necessary ==="
LoadCan

# 3. --- Создадим для тестрования ${NUMBER_OF_CHANNELS} каналов.
echo "=== Find unused  UDP ports ==="
unusedPorts=( $(GetUnusedPorts $(( NUMBER_OF_CHANNELS * 2 )) ) )
TxUdpPorts=(${unusedPorts[@]:0:${NUMBER_OF_CHANNELS}})
RxUdpPorts=(${unusedPorts[@]:${NUMBER_OF_CHANNELS}})

echo "=== Find unused CAN interfaces ==="
CanIfs=( $(GetUnusedCanInfs $NUMBER_OF_CHANNELS) )

mkdir -p out
rm -rf out/*

# 4. --- Запустим средства получения данных из слушающих концов каналов
echo "=== Launch netcat for UDP listening ==="
for p in "${RxUdpPorts[@]}"; do 
    echo -n "   nc -ulk $p > out/udp${p}_.txt &"
    netcat -ulk $p > out/udp${p}_.txt &
    pid=$!
    echo " --- [$pid]"
    BG_PIDS+=($pid)
done

echo "=== Create CAN interfaces and launch candump for CAN listening ==="
for i in "${CanIfs[@]}"; do
    sudo ip link add dev vcan$i type vcan
    sudo ip link set up vcan$i
    echo -n "   candump -L vcan$i | cut -d' ' -f3 > out/can${i}_.txt &"
    candump -L vcan$i > out/can${i}_.txt &
    pid=$!
    echo " --- [$pid]"
    BG_PIDS+=($pid)
done

# 5. --- Запустим тестируемое приложение
echo "=== Launch udp2can ==="
args=()
for ((i = 0 ; i < $NUMBER_OF_CHANNELS ; i++)); do
    args+=("${TxUdpPorts[$i]}:vcan${CanIfs[$i]}:127.0.0.1:${RxUdpPorts[$i]}")
done
echo -n "   ./udp2can ${args[@]} &"
./udp2can ${args[@]} &
pid=$!
echo " --- [$pid]"
BG_PIDS+=($pid)

# 6. --- Сгенерируем тестовые транзакции, по NUMBER_OF_PACKETS для каждого конца канала.
# рандомизировать будем длину полезных данных в пакете, а так же очерёдность передачи пакетов.
randomLength=$(cat /dev/urandom | tr -dc '0-8' | head --bytes $NUMBER_OF_PACKETS | fold -w1)
Transactions=()
# Для udp транзакция имеет вид:
# 4 байта - id+flags, 1 байт - длина, 3 байта - нули, дальше идёт n ненулевых байтов ( по размеру полезной нагрузки),
# после чего идет ( 8 - n ) нулевых байтов.
echo "=== Generate UDP packets ==="
udpPacket="\x01\x23\x45\x67\x89\xab\xcd\xef"
udpZeroes="\x00\x00\x00\x00\x00\x00\x00\x00"
for p in "${TxUdpPorts[@]}"; do
    for l in $randomLength; do
        Transactions+=( "echo -en \"\x01\x00\x00\x00\x0${l}\x00\x00\x00${udpPacket:0:(( l * 4 ))}${udpZeroes:0:(( ( 8 - l ) * 4 ))}\" > /dev/udp/localhost/$p" )
    done 
done

# Для can транзакция имеет вид:
# 001#0123456789abcdef, полезная нагрузка будет обрезаться в зависимости от длины пакета
echo "=== Generate CAN packets ==="
canPacket="001#0123456789abcdef"
for i in "${CanIfs[@]}"; do
    for l in $randomLength; do
        Transactions+=("cansend vcan${i} ${canPacket:0:(( 4 + (l * 2) ))}")
    done 
done

# Перемешаем пакеты
echo "=== Shuffle UDP and CAN packets ==="
readarray -t TransactionsShuf <<< "$(printf "%s\n" "${Transactions[@]}" | shuf)"

# 7. --- Выполним передачу пакетов
echo -e "=== Send packets ===\n"
for p in "${TransactionsShuf[@]}"; do
    echo ">>>   $p"
    eval "${p}"
done
echo ""

# 8. --- Переводим данные в аналогичный can вид для удобства сравнения
echo "=== Postprocessing UDP data ==="
for p in "${RxUdpPorts[@]}"; do
    Fail=0
    outfile=out/udp${p}.txt
    while IFS= read -r line; do
        # Проверим заголовок
        [[ "${line:0:8}" == "01000000" ]] && echo -n "001#" >> $outfile || Fail=1
        len=${line:8:2}
        # Выводим полезные данные
        echo -n "${line:16:(( ${len#0} * 2 ))}" >> $outfile
        [[ "$Fail" == 1 ]] && echo "_FAIL" >> $outfile || echo "" >> $outfile
    done < <( xxd -u -g 16 out/udp${p}_.txt | cut -d' ' -f2 )
    rm out/udp${p}_.txt
    # Отсортируем вывод, чтобы было удобно сравнивать результаты работы теста
    cat $outfile | sort > ${outfile}_ && mv ${outfile}_ ${outfile}
done
for i in "${CanIfs[@]}"; do
    outfile=out/can${i}.txt
    cat out/can${i}_.txt | cut -d' ' -f3 > ${outfile}
    rm out/can${i}_.txt
    # Отсортируем вывод и удалим каждую вторую строчку, так как из за эха имеет место дуюлирования пакетов
    cat ${outfile} | sort | sed -e n\;d > ${outfile}_ && mv ${outfile}_ ${outfile}
done

EndTest

# 9. --- Сравниваем файл за файлом на предмет идентичности
Fail=0
res=( $(find out -type f) )
for(( i=0; i<(( ${#res[@]}-1 )); i++ )); do
    cmp ${res[$i]} ${res[(( i+1 ))]} &> /dev/null || { Fail=1; echo "Error: ${res[$i]} and ${res[(( i+1 ))]} don't match"; }
done
[[ "$Fail" == "0" ]] && echo "=== Test passed! ===" || echo "=== Test failed!=== "
exit $Fail